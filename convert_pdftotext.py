import os
from os import listdir
from os.path import isfile, join
import subprocess
import re
import shutil


def import_doc(fullpath):
    with open(fullpath, 'r', encoding='utf-8') as f:
        lines = f.readlines()
    doc = ''.join(lines)
    doc = doc.lower()
    doc = re.sub(r'[\W_]', '', doc)
    doc = doc.replace('\n', ' ')
    return doc


def read_docs():
    baseDir='/marleen'
    docs = {}
    base_dir = '.' + baseDir + '/pdf'
    files = [f for f in listdir(base_dir) if isfile(join(base_dir, f))]
    files.sort()
    for file in files:
        fullpath = '\'' + os.path.join(base_dir, file) + '\''
        cmd = 'pdftotext ' + fullpath
        print('1/2', cmd)
        subprocess.call(cmd, shell=True)

        txt_file = file.replace('.pdf', '.txt')
        source = os.path.join(base_dir, txt_file)
        destination = os.path.join('.' + baseDir + '/txt', txt_file)
        shutil.move(source, destination)

    # return txt


def count_words(terms, docs):
    for filename in docs:
        doc = docs[filename]
        print(filename)
        total = 0
        for term in terms:
            times = doc.count(term)
            total += times
            print('#####', term, times)
        print('===== total', total)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    files = read_docs()
    print(files)
