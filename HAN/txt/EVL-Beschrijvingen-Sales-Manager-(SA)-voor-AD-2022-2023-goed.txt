Datum: 7 juli 2022
Pagina: 218 van 294
Omvat de leeruitkomst(en)

Opstellen van een adviesrapport

Tentamenvorm/ vormen

Adviesrapport met CGI, individueel

Tentamenmoment

Regulier: L2 / L4
Herkansing iom docent

Beoordelingscriteria

De beoordelingscriteria zijn nader uitgewerkt in de handleiding en het
beoordelingsformulier op onderwijsonline.

Minimaal oordeel deeltentamen

5,5

Minimaal oordeel EVL

6

Sales Manager
SA

Sales Manager (max 120 posities in OSIRIS)

Naam module Engelstalig

<maximaal 120 posities in OSIRIS>

Ingangseisen

Voor de opleiding Bachelor Commerciële Economie / Associate Degree
Commerciële Economie duaal/deeltijd / Associate Degree Online Marketing
& Sales duaal/deeltijd is het verplicht een relevante werkplek te hebben. De
werkplek wordt beoordeeld aan de hand van een werkplekscan. Voldoet de
werkplek niet aan de eisen van de werkplekscan, is het niet mogelijk deze
module te volgen. De opleiding beoordeelt of de werkplek voldoet aan de
eisen van de opleiding.
Een duaal student dient te beschikken over een arbeids- of
stageovereenkomst.
Een deeltijd student dient te beschikken over een arbeids-, of
stageovereenkomst of bewijs van vrijwilligerswerk.
De professional skills worden uitgevoerd bij het bedrijf waar de duaal of
deeltijd beginnend beroepsbeoefenaar werkzaam is. Naast werkzaamheden
op een marketing- of salesafdeling worden een of meer relevante
opdrachten zelfstandig uitgevoerd.
Overige ingangseisen:
• studievoortgangsnorm behaald,
• Ad CE/OMS: minimaal 75 studiepunten behaald
• behaalde EvL’en:
o Professional skills van JM, BS en AC en
o ACVP,
• maximaal 3 toetsen open (openstaande toetsen moeten tijdens module
afgerond kunnen worden).
Eventuele uitzonderingen hierop zijn altijd in overleg met en na
goedkeuring van de studieadviseur.

Naam EVL

Aantal
studiepunten

Overzicht van EVL’en

1

SAJB Jaarbudget

5

waaruit de module is

2

SAAR Adviesrapport

5

opgebouwd

3

SAVG Voortgangsgesprek

2,5

4

SATO Teamoverleg

2,5

5

SAEV Evaluatieopdracht

15

Deelnameplicht onderwijs
Taal indien anders dan
Nederlands
Inrichtingsvorm

Nee
Niet van toepassing
Deeltijd / Duaal

125

Datum: 7 juli 2022
Pagina: 219 van 294
Bachelor Commerciële Economie
Afstudeerrichting/
afstudeervariant

Associate Degree Commerciële Economie / Online Marketing & Sales:
uitstroomdifferentiatie Sales

Honoursprogramma/
talentenprogramma
Beschrijving van de context van deze module
De beginnend beroepsbeoefenaar functioneert in een MKB of groot bedrijf. Hij/zij werkt in een
multidisciplinaire
commerciële setting zelfstandig en zelfsturend. De situatie en taken zijn complex en (deels) gestructureerd.
Hij/zij past beproefde standaardmethoden toe die gebruikelijk zijn in de branche. De beginnend
beroepsbeoefenaar gebruikt hierbij theoretische kennis en vaardigheden en past deze aan de praktijk aan.
De beginnende beroepsbeoefenaar werkt afdelingsoverstijgend in verschillende projectte ams.
De beginnend beroepsbeoefenaar oriënteert zich op beroep en functie, past theoretische kennis toe op
commercieel- economisch gebied, oefent en ontwikkelt de skills van de commercieel professional, verkrijgt
een beeld van de werkzaamheden en werksituaties van medewerkers, verkrijgt een beeld van de structuur
en het functioneren van een arbeidsorganisatie.

126

Datum: 7 juli 2022
Pagina: 220 van 294

SAJB Jaarbudget
EVL 1 – SAJB Jaarbudget [maximaal 120 posities in OSIRIS]
Naam EVL lang EN

[maximaal 120 posities in OSIRIS]

Naam EVL kort NL

[maximaal 40 posities in OSIRIS]

Naam EVL kort EN

[maximaal 40 posities in OSIRIS]

Naam EVL Alluris

Jaarbudget

Code EVL OSIRIS
Code EVL Alluris

Vermeld de code die in OSIRIS aan deze eenheid van leeruitkomsten is
gekoppeld. [XXXXXnn (6 letterige code + 2 cijfers in het volgnummer)]
SAJB
Opleidingsprofiel CE 2018-2022

Eindkwalificatie(s)

Koers bepalen

x

Business Development

x

Waarde creëren
Realiseren
Aantal studiepunten
Ingangseisen EVL
Intekenen onderwijsarsenaal

5
De ingangseisen zijn op module niveau geformuleerd, niet op EVL
niveau.
Voor het onderwijsarsenaal dat wordt aangeboden na 31 januari 2023
geldt dat het noodzakelijk is dat studenten zich aanmelden voor het
onderwijs dat zij willen volgen (ook wel ‘intekenen’ genoemd). Zie Deel 3
‘Regeling onderwijs en (deel)tentamens OSIRIS’ voor meer informatie.

Beschrijving van de context van deze EVL
De beginnend beroepsbeoefenaar functioneert in een MKB of groot bedrijf. Hij/zij werkt in een
multidisciplinaire commerciële setting zelfstandig en zelfsturend. De situatie en taken zijn complex en (deels)
gestructureerd. Hij/zij past beproefde standaardmethoden toe die gebruikelijk zijn in de branche. De
beginnend beroepsbeoefenaar gebruikt hierbij theoretische kennis en vaardigheden en past deze aan de
praktijk aan. De beginnende beroepsbeoefenaar werkt afdeling overstijgend in verschillende projectteams.
De beginnend beroepsbeoefenaar oriënteert zich op beroep en functie, past theoretische kenn is toe op
commercieel- economisch gebied, oefent en ontwikkelt de skills van de commercieel professional, verkrijgt
een beeld van de werkzaamheden en werksituaties van medewerkers, verkrijgt een beeld van de structuur
en het functioneren van een arbeidsorganisatie
Beschrijving van de leeruitkomst(en) waaruit deze EVL is opgebouwd

Opstellen van een jaarbudget

De beginnend beroepbeoefenaar
• kan een waarnemend onderzoek uitvoeren bij meerdere retailvestigingen;
• kan op basis van marktonderzoek, afzetgegevens en forecast het budget
voor komend jaar vaststellen;
• kan de afzetgegevens vertalen in een complex Excelbestand waarin
tevens kosten en opbrengsten zijn opgenomen, resulterend in
een profit & loss overzicht;
• kan een duidelijk verkoop- en promotiebeleid vaststellen waarmee de
doelstellingen gerealiseerd kunnen worden.

127

Datum: 7 juli 2022
Pagina: 221 van 294

De beginnend beroepsbeoefenaar is in staat een overtuigende
mondelinge presentatie te geven over het jaarbudget voor komend jaar.
Hij/zij weet de senior manager ervan te overtuigen het budget voor
komend jaar naar beneden bij te stellen en weet goed aan te geven met
welk te voeren beleid de nieuwe targets kunnen worden gerealiseerd.
Presenteren

In de presentatie wordt een juiste afweging gemaakt van
presentatiestijlen en inhoud gerelateerd aan het doel en publiek; wordt
zichtbaar aandacht besteed aan een passende opening en een passend
slot; worden de gebruikte hulpmiddelen op een juiste manier ingezet; zijn
de gebruikte non-verbale en verbale communicatiemiddelen passend,
zakelijk en professioneel.

TENTAMINERING
Deeltentamen 1
Naam (deel)tentamen NL

<maximaal 120 posities in OSIRIS>

Naam (deel)tentamen EN

<maximaal 120 posities in OSIRIS>

Code (deel)tentamen OSIRIS

Vermeld het volgnummer van het tentamen of deeltentamen in OSIRIS.

Code en naam (deel)tentamen

SAJB0A.5_D

Alluris

Jaarbudget

Wijze van aanmelden voor
(deel)tentamengelegenheden /
aanmeldingstermijn

Niet van toepassing

t/m 31 januari 2023
(via Alluris)
Intekenen en uittekenen voor
(deel)tentamengelegenheden
vanaf 1 februari 2023
(via OSIRIS)

Beschrijf de wijze van intekenen en uittekenen voor
(deel)tentamengelegenheden aangeboden vanaf 1 februari 2023
(implementatie Osiris) en bijbehorende termijnen. Voor meer informatie
zie Deel 3 ‘Regeling onderwijs en (deel)tentamens OSIRIS’.

Toegestane hulpmiddelen

Zie studiehandleiding op #OnderwijsOnline

Weging

50%

Omvat de leeruitkomst(en)

Opstellen van een jaarbudget

Tentamenvorm/ vormen

Inleveropdracht, individueel

Tentamenmoment

Regulier L1 / L3
Herkansing i.o.m. docent
De beginnend beroepsbeoefenaar is in staat de bevindingen uit het
waarnemend onderzoek te vertalen naar relevante conclusies voor het
op te stellen jaarbudget. De beginnend beroepsbeoefenaar stelt op basis

Beoordelingscriteria

van onderzoek en analyse een verkoopbudget voor het komend jaar op.
De bijbehorende Excel file is volledig en juist ingevuld. De in het budget
opgenomen targets zijn voldoende onderbouwd en toegelicht. Er wordt
uitgebreid toegelicht met welk sales- en promotiebeleid de doelstellingen
worden gerealiseerd.

128

Datum: 7 juli 2022
Pagina: 222 van 294

De beoordelingscriteria zijn nader uitgewerkt in de handleiding en het
beoordelingsformulier op #OnderwijsOnline.
Minimaal oordeel deeltentamen

5,5

Deeltentamen 2
Naam (deel)tentamen NL

<maximaal 120 posities in OSIRIS>

Naam (deel)tentamen EN

<maximaal 120 posities in OSIRIS>

Code (deel)tentamen OSIRIS

Vermeld het volgnummer van het tentamen of deeltentamen in OSIRIS.

Code en naam (deel)tentamen

SAJBP0A.6_D

Alluris

Pitch

Wijze van aanmelden voor
(deel)tentamengelegenheden /
aanmeldingstermijn

Niet van toepassing

t/m 31 januari 2023
(via Alluris)
Intekenen en uittekenen voor

Beschrijf de wijze van intekenen en uittekenen voor

(deel)tentamengelegenheden

(deel)tentamengelegenheden aangeboden vanaf 1 februari 2023

vanaf 1 februari 2023

(implementatie Osiris) en bijbehorende termijnen. Voor meer informatie

(via OSIRIS)

zie Deel 3 ‘Regeling onderwijs en (deel)tentamens OSIRIS’.

Toegestane hulpmiddelen

Zie studiehandleiding op #OnderwijsOnline

Weging

50%

Omvat de leeruitkomst(en)

Presenteren

Tentamenvorm/ vormen

Presentatie, individueel

Tentamenmoment

Regulier L1 / L3
Herkansing i.o.m. docent

Beoordelingscriteria

De beginnend beroepsbeoefenaar houdt een overtuigende korte
presentatie waarin hij/zij de senior manager weet te overtuigen van de
juiste aanpak om de doelen uit het jaarbudget voor komend jaar te
realiseren.
De beoordelingscriteria zijn nader uitgewerkt in de handleiding en het
beoordelingsformulier op onderwijsonline.
De inhoud van de presentatie betreft het jaarbudget dat bij jaarbudget
wordt opgesteld.

Minimaal oordeel deeltentamen

5,5

Minimaal oordeel EVL

6

129

Datum: 7 juli 2022
Pagina: 223 van 294

SAAR Adviesrapport
EVL 2 – SAAR Adviesrapport [maximaal 120 posities in OSIRIS]
Naam EVL lang EN

[maximaal 120 posities in OSIRIS]

Naam EVL kort NL

[maximaal 40 posities in OSIRIS]

Naam EVL kort EN

[maximaal 40 posities in OSIRIS]

Naam EVL Alluris

Adviesrapport

Code EVL OSIRIS
Code EVL Alluris

Vermeld de code die in OSIRIS aan deze eenheid van leeruitkomsten is
gekoppeld. [XXXXXnn (6 letterige code + 2 cijfers in het volgnummer)]
SAAR
Opleidingsprofiel CE 2018-2022
Koers bepalen

Eindkwalificatie(s)

x

Business Development
Waarde creëren

x

Realiseren
Aantal studiepunten
Ingangseisen EVL
Intekenen onderwijsarsenaal

5
De ingangseisen zijn op module niveau geformuleerd, niet op EVL
niveau.
Voor het onderwijsarsenaal dat wordt aangeboden na 31 januari 2023
geldt dat het noodzakelijk is dat studenten zich aanmelden voor het
onderwijs dat zij willen volgen (ook wel ‘intekenen’ genoemd). Zie Deel 3
‘Regeling onderwijs en (deel)tentamens OSIRIS’ voor meer informatie.

Beschrijving van de context van deze EVL
De beginnend beroepsbeoefenaar functioneert in een MKB of groot bedrijf. Hij/zij werkt in een
multidisciplinaire commerciële setting zelfstandig en zelfsturend. De situatie en taken zijn complex en (deels)
gestructureerd. Hij/zij past beproefde standaardmethoden toe die gebruikelijk zijn in de branche. De
beginnend beroepsbeoefenaar gebruikt hierbij theoretische kennis en vaardigheden en past deze aan de
praktijk aan. De beginnende beroepsbeoefenaar werkt afdeling overstijgend in versch illende projectteams.
De beginnend beroepsbeoefenaar oriënteert zich op beroep en functie, past theoretische kennis toe op
commercieel- economisch gebied, oefent en ontwikkelt de skills van de commercieel professional, verkrijgt
een beeld van de werkzaamheden en werksituaties van medewerkers, verkrijgt een beeld van de structuur
en het functioneren van een arbeidsorganisatie.
Beschrijving van de leeruitkomst(en) waaruit deze EVL is opgebouwd
De beginnend beroepsbeoefenaar is in staat om een advies uit te
brengen over verbeteringen in het salesproces. Hierbij is de beginnend

Adviesrapport

beroepsbeoefenaar in staat om:
• de huidige situatie in kaart te brengen middels de salesfunnel;
• de urgentie van het probleem aan te geven middels feiten en cijfers;
• de problematiek kernachtig samen te vatten in een zelf gecreëerd;
managementmodel;
• oplossingen te formuleren voor het vraagstuk en deze uit te werken in
• activiteiten, planning, kosten en opbrengsten en meetbare KPI’s;

130

Datum: 7 juli 2022
Pagina: 224 van 294
• hierover duidelijk en volledig te rapporteren in ppt-format.
De beginnend beroepsbeoefenaar is in staat om een rapport
in ppt format te maken. Dit rapport is geformuleerd in correct
Rapporteren Nederlands

Nederlands, is visueel aantrekkelijk (vormgeving en opmaak), is
zelfstandig leesbaar en de kopregels bevatten de samenvatting van
de dia’s.

TENTAMINERING
Deeltentamen 1
Naam (deel)tentamen NL

<maximaal 120 posities in OSIRIS>

Naam (deel)tentamen EN

<maximaal 120 posities in OSIRIS>

Code (deel)tentamen OSIRIS

Vermeld het volgnummer van het tentamen of deeltentamen in OSIRIS.

Code en naam (deel)tentamen

SAARPP0A.5_D

Alluris

Advies

Wijze van aanmelden voor
(deel)tentamengelegenheden /
aanmeldingstermijn

Niet van toepassing

t/m 31 januari 2023
(via Alluris)
Intekenen en uittekenen voor
(deel)tentamengelegenheden
vanaf 1 februari 2023
(via OSIRIS)

Beschrijf de wijze van intekenen en uittekenen voor
(deel)tentamengelegenheden aangeboden vanaf 1 februari 2023
(implementatie Osiris) en bijbehorende termijnen. Voor meer informatie
zie Deel 3 ‘Regeling onderwijs en (deel)tentamens OSIRIS’.

Toegestane hulpmiddelen

Zie studiehandleiding op #OnderwijsOnline

Weging

50%

Omvat de leeruitkomst(en)

Adviesrapport

Tentamenvorm/ vormen

Inleveropdracht, individueel

Tentamenmoment

Regulier L1 / L3
Herkansing i.o.m. docent
De beginnend beroepsbeoefenaar kan advies uitbrengen over
verbeteringen in het salesproces. De beginnend beroepsbeoefenaar kan
de huidige situatie in kaart brengen, knelpunten bepalen en tot creatieve
oplossingen komen. Hij/zij is in staat daarover duidelijk en volledig te in

Beoordelingscriteria

ppt-format.
De beoordelingscriteria zijn nader uitgewerkt in de handleiding en het
beoordelingsformulier op #OnderwijsOnline.

Minimaal oordeel deeltentamen

5,5

Deeltentamen 2
Naam (deel)tentamen NL

<maximaal 120 posities in OSIRIS>

131

Datum: 7 juli 2022
Pagina: 225 van 294
Naam (deel)tentamen EN

<maximaal 120 posities in OSIRIS>

Code (deel)tentamen OSIRIS

Vermeld het volgnummer van het tentamen of deeltentamen in OSIRIS.

Code en naam (deel)tentamen

SAARRAP0A.5_D

Alluris

Rapport in PPT

Wijze van aanmelden voor
(deel)tentamengelegenheden /
aanmeldingstermijn

Niet van toepassing

t/m 31 januari 2023
(via Alluris)
Intekenen en uittekenen voor

Beschrijf de wijze van intekenen en uittekenen voor

(deel)tentamengelegenheden

(deel)tentamengelegenheden aangeboden vanaf 1 februari 2023

vanaf 1 februari 2023

(implementatie Osiris) en bijbehorende termijnen. Voor meer informatie

(via OSIRIS)

zie Deel 3 ‘Regeling onderwijs en (deel)tentamens OSIRIS’.

Toegestane hulpmiddelen

Zie studiehandleiding op #OnderwijsOnline

Weging

50%

Omvat de leeruitkomst(en)

Rapporteren

Tentamenvorm/ vormen

Inleveropdracht, individueel

Tentamenmoment

Regulier L1 / L3

Beoordelingscriteria

De beginnend beroepsbeoefenaar is in staat om een rapport

Herkansing i.o.m. docent
in ppt format te maken. Dit rapport is geformuleerd in correct
Nederlands, is visueel aantrekkelijk (vormgeving en opmaak), is
zelfstandig leesbaar en de kopregels bevatten de samenvatting van de
slides.
De beoordelingscriteria zijn nader uitgewerkt in de handleiding en het
beoordelingsformulier op #OnderwijsOnline.
De inhoud van het rapport wordt opgesteld bij adviesrapport in ppt.
Minimaal oordeel deeltentamen

5,5

Minimaal oordeel EVL

6

132

Datum: 7 juli 2022
Pagina: 226 van 294

SAVG Voortgangsgesprek
EVL 3 – SAVG Voortgangsgesprek [maximaal 120 posities in OSIRIS]
Naam EVL lang EN

[maximaal 120 posities in OSIRIS]

Naam EVL kort NL

[maximaal 40 posities in OSIRIS]

Naam EVL kort EN

[maximaal 40 posities in OSIRIS]

Naam EVL Alluris

Voortgangsgesprek

Code EVL OSIRIS
Code EVL Alluris

Vermeld de code die in OSIRIS aan deze eenheid van leeruitkomsten is
gekoppeld. [XXXXXnn (6 letterige code + 2 cijfers in het volgnummer)]
SAVG
Opleidingsprofiel CE 2018-2022

Eindkwalificatie(s)

Koers bepalen

x

Business Development
Waarde creëren
Realiseren

Aantal studiepunten
Ingangseisen EVL
Intekenen onderwijsarsenaal

x

2,5
De ingangseisen zijn op module niveau geformuleerd, niet op EVL
niveau.
Voor het onderwijsarsenaal dat wordt aangeboden na 31 januari 2023
geldt dat het noodzakelijk is dat studenten zich aanmelden voor het
onderwijs dat zij willen volgen (ook wel ‘intekenen’ genoemd). Zie Deel 3
‘Regeling onderwijs en (deel)tentamens OSIRIS’ voor meer informatie.

Beschrijving van de context van deze EVL
De beginnend beroepsbeoefenaar functioneert in een MKB of groot bedrijf. Hij/zij werkt in een
multidisciplinaire commerciële setting zelfstandig en zelfsturend. De situatie en taken zijn complex en (deels)
gestructureerd. Hij/zij past beproefde standaardmethoden toe die gebruikelijk zijn in de branche. De
beginnend beroepsbeoefenaar gebruikt hierbij theoretische kennis en vaardigheden en past deze aan de
praktijk aan. De beginnende beroepsbeoefenaar werkt afdeling overstijgend in verschillende projectteams.
De beginnend beroepsbeoefenaar oriënteert zich op beroep en functie, past theoretische kennis toe op
commercieel- economisch gebied, oefent en ontwikkelt de skills van de commercieel professional, verkrijgt
een beeld van de werkzaamheden en werksituaties van medewerkers, verkrijgt een beeld van de structuur
en het functioneren van een arbeidsorganisatie
Beschrijving van de leeruitkomst(en) waaruit deze EVL is opgebouwd
De beginnende beroepsbeoefenaar heeft het vermogen om op een
stimulerende wijze leiding te geven aan salesprofessionals. Hij/zij kan de
Voeren van een voortgangsg

prestaties van een salesmedewerker beoordelen aan de hand van

esprek

rapportages, weet de belangen en interesses van verschillende partijen
met elkaar te verbinden, weet medewerkers te stimuleren, te coachen
en zo nodig te corrigeren.

TENTAMINERING

133

Datum: 7 juli 2022
Pagina: 227 van 294

Deeltentamen 1
Naam (deel)tentamen NL

<maximaal 120 posities in OSIRIS>

Naam (deel)tentamen EN

<maximaal 120 posities in OSIRIS>

Code (deel)tentamen OSIRIS

Vermeld het volgnummer van het tentamen of deeltentamen in OSIRIS.

Code en naam (deel)tentamen

SAVGGES0A.4_D

Alluris

Voortgangsgesprek

Wijze van aanmelden voor
(deel)tentamengelegenheden /
aanmeldingstermijn

Niet van toepassing

t/m 31 januari 2023
(via Alluris)
Intekenen en uittekenen voor
(deel)tentamengelegenheden
vanaf 1 februari 2023
(via OSIRIS)

Beschrijf de wijze van intekenen en uittekenen voor
(deel)tentamengelegenheden aangeboden vanaf 1 februari 2023
(implementatie Osiris) en bijbehorende termijnen. Voor meer informatie
zie Deel 3 ‘Regeling onderwijs en (deel)tentamens OSIRIS’.

Toegestane hulpmiddelen

Zie studiehandleiding op #OnderwijsOnline

Weging

100%

Omvat de leeruitkomst(en)

Voeren van een voortgangsgesprek

Tentamenvorm/ vormen

Mondeling, individueel

Tentamenmoment

Regulier L2 / L4
Herkansing i.o.m. docent
De beginnend beroepsbeoefenaar:

• kan de prestaties van een salesmedewerker beoordelen aan de hand
van rapportages;

• kan een voortgangsgesprek houden waarin hij/zij zowel corrigerend als
Beoordelingscriteria

motiverend tov de salesmedewerker optreedt, gebruikmakend van
relevante modellen en theorieën;
• kan de gemaakte afspraken op een juiste wijze schriftelijk vastleggen.
De beoordelingscriteria zijn nader uitgewerkt in de handleiding en het
beoordelingsformulier op #OnderwijsOnline.

Minimaal oordeel deeltentamen

5,5

Minimaal oordeel EVL

6

134

Datum: 7 juli 2022
Pagina: 228 van 294

SATO Teamoverleg
EVL 4 – SATO Teamoverleg [maximaal 120 posities in OSIRIS]
Naam EVL lang EN

[maximaal 120 posities in OSIRIS]

Naam EVL kort NL

[maximaal 40 posities in OSIRIS]

Naam EVL kort EN

[maximaal 40 posities in OSIRIS]

Naam EVL Alluris

Teamoverleg

Code EVL OSIRIS
Code EVL Alluris

Vermeld de code die in OSIRIS aan deze eenheid van leeruitkomsten is
gekoppeld. [XXXXXnn (6 letterige code + 2 cijfers in het volgnummer)]
SATO
Opleidingsprofiel CE 2018-2022

Eindkwalificatie(s)

Koers bepalen
Business Development

x

Waarde creëren
Realiseren
Aantal studiepunten
Ingangseisen EVL
Intekenen onderwijsarsenaal

x

2,5
De ingangseisen zijn op module niveau geformuleerd, niet op EVL
niveau.
Voor het onderwijsarsenaal dat wordt aangeboden na 31 januari 2023
geldt dat het noodzakelijk is dat studenten zich aanmelden voor het
onderwijs dat zij willen volgen (ook wel ‘intekenen’ genoemd). Zie Deel 3
‘Regeling onderwijs en (deel)tentamens OSIRIS’ voor meer informatie.

Beschrijving van de context van deze EVL
De beginnend beroepsbeoefenaar functioneert in een MKB of groot bedrijf. Hij/zij werkt in een
multidisciplinaire commerciële setting zelfstandig en zelfsturend. De situatie en taken zijn complex en (deels)
gestructureerd. Hij/zij past beproefde standaardmethoden toe die gebruikelijk zijn in de branche. De
beginnend beroepsbeoefenaar gebruikt hierbij theoretische kennis en vaardigheden en past deze aan de
praktijk aan. De beginnende beroepsbeoefenaar werkt afdeling overstijgend in versch illende projectteams.
De beginnend beroepsbeoefenaar oriënteert zich op beroep en functie, past theoretische kennis toe op
commercieel- economisch gebied, oefent en ontwikkelt de skills van de commercieel professional, verkrijgt
een beeld van de werkzaamheden en werksituaties van medewerkers, verkrijgt een beeld van de structuur
en het functioneren van een arbeidsorganisatie. .
Beschrijving van de leeruitkomst(en) waaruit deze EVL is opgebouwd
De beginnend beroepsbeoefenaar voert een effectief
(onderhandelings)gesprek in het Engels met buitenlandse collega’s
waarbij de juiste uitspraak wordt gehanteerd, vloeiend wordt gesproken,
Onderhandelen in het

de juiste grammatica wordt toegepast en zakelijk jargon wordt gebruikt.

Engels

De beginnend beroepsbeoefenaar toont daarbij begrip voor culturele
verscheidenheid.
Taalniveau B2

135

Datum: 7 juli 2022
Pagina: 229 van 294

TENTAMINERING
Deeltentamen 1
Naam (deel)tentamen NL

<maximaal 120 posities in OSIRIS>

Naam (deel)tentamen EN

<maximaal 120 posities in OSIRIS>

Code (deel)tentamen OSIRIS

Vermeld het volgnummer van het tentamen of deeltentamen in OSIRIS.

Code en naam (deel)tentamen

SATOTMO0A.4_D

Alluris

Teamoverleg

Wijze van aanmelden voor
(deel)tentamengelegenheden /
aanmeldingstermijn

Niet van toepassing

t/m 31 januari 2023
(via Alluris)
Intekenen en uittekenen voor
(deel)tentamengelegenheden
vanaf 1 februari 2023
(via OSIRIS)

Beschrijf de wijze van intekenen en uittekenen voor
(deel)tentamengelegenheden aangeboden vanaf 1 februari 2023
(implementatie Osiris) en bijbehorende termijnen. Voor meer informatie
zie Deel 3 ‘Regeling onderwijs en (deel)tentamens OSIRIS’.

Toegestane hulpmiddelen

Zie studiehandleiding op #OnderwijsOnline

Weging

100%

Omvat de leeruitkomst(en)

Onderhandelen in het Engels

Tentamenvorm/ vormen

Mondeling, individueel

Tentamenmoment

Regulier L2 / L4
Herkansing iom docent
De beginnend beroepsbeoefenaar voert een effectief
(onderhandelings)gesprek in het Engels met buitenlandse collega’s
waarbij de juiste uitspraak wordt gehanteerd, vloeiend wordt gesproken,
de juiste grammatica wordt toegepast en zakelijk jargon wordt gebruikt.
De beginnend beroepsbeoefenaar toont daarbij begrip voor culturele

Beoordelingscriteria

verscheidenheid.
Taalniveau B2
De beoordelingscriteria zijn nader uitgewerkt in de handleiding en het
beoordelingsformulier op #OnderwijsOnline.

Minimaal oordeel deeltentamen

5,5

Minimaal oordeel EVL

6

136

Datum: 7 juli 2022
Pagina: 230 van 294

SAEV Evaluatieopdracht
EVL 5 – SAEV Evaluatieopdracht Sales [maximaal 120 posities in OSIRIS]
Naam EVL lang EN

[maximaal 120 posities in OSIRIS]

Naam EVL kort NL

[maximaal 40 posities in OSIRIS]

Naam EVL kort EN

[maximaal 40 posities in OSIRIS]

Naam EVL Alluris

Evaluatieopdracht Sales

Code EVL OSIRIS
Code EVL Alluris

Vermeld de code die in OSIRIS aan deze eenheid van leeruitkomsten is
gekoppeld. [XXXXXnn (6 letterige code + 2 cijfers in het volgnummer)]
SAEV
Opleidingsprofiel CE 2018-2022

Eindkwalificatie(s)

Aantal studiepunten
Ingangseisen EVL
Intekenen onderwijsarsenaal

Koers bepalen

x

Business Development

x

Waarde creëren

x

Realiseren

x

15
De ingangseisen zijn op module niveau geformuleerd, niet op EVL
niveau.
Voor het onderwijsarsenaal dat wordt aangeboden na 31 januari 2023
geldt dat het noodzakelijk is dat studenten zich aanmelden voor het
onderwijs dat zij willen volgen (ook wel ‘intekenen’ genoemd). Zie Deel 3
‘Regeling onderwijs en (deel)tentamens OSIRIS’ voor meer informatie.

Beschrijving van de context van deze EVL
De beginnend beroepsbeoefenaar functioneert in een MKB of groot bedrijf. Hij/zij werkt in een
multidisciplinaire commerciële setting zelfstandig en zelfsturend. De situatie en taken zijn complex en (deels)
gestructureerd. Hij/zij past beproefde standaardmethoden toe die gebruikelijk zijn in de branche. De
beginnend beroepsbeoefenaar gebruikt hierbij theoretische kennis en vaardigheden en past deze aan de
praktijk aan. De beginnende beroepsbeoefenaar werkt afdeling overstijgend in verschillende projectteams.
De beginnend beroepsbeoefenaar oriënteert zich op beroep en functie, past theoretische kennis toe op
commercieel- economisch gebied, oefent en ontwikkelt de skills van de commercieel professional, verkrijgt
een beeld van de werkzaamheden en werksituaties van medewerkers, verkrijgt een beeld van de structuur
en het functioneren van een arbeidsorganisatie.
Beschrijving van de leeruitkomst(en) waaruit deze EVL is opgebouwd
De beginnend beroepsbeoefenaar functioneert in een MKB of groot
bedrijf in een commerciële setting. Hierbij heeft hij/zij heeft oog voor
verschillende belangen binnen het bedrijf en kan draagvlak creëren.
Werken en reflecteren op de

Hij/zij communiceert goed, zowel verbaal als schriftelijk. Hij/zij heeft oog

werkplek

voor detail en plant zijn eigen werk zorgvuldig zodat zijn werk af is. Hij/zij
is in staat onder hoge druk en binnen gestelde deadlines naar het
resultaat toe te werken binnen en gestructureerde context en met
voldoende begeleiding. Hij/zij is sensitief voor het maken van de

137

Datum: 7 juli 2022
Pagina: 231 van 294

verbinding op inhoud- en mensniveau binnen zijn eigen omgeving. Hij
beschikt over een ondernemende houding.
De beginnend beroepsbeoefenaar kan zijn/haar droombaan over 3 jaar
beschrijven en weet daarbij aan te geven welke van de onderstaande
skills uit het opleidingsprofiel CE het meest van toepassing zijn. Het gaat
hierbij om de skills:
• Kritisch denken / probleemoplossend vermogen
• Creativiteit
• Communicatie
• Samenwerken
• Nieuwsgierigheid
• Initiatief
• Doorzettingsvermogen
• aanpassingsvermogen
• Leiderschap
• Verantwoordelijkheidsbesef
• Commercieel bewustzijn
Hij/zij werkt doelbewust aan verdere ontwikkeling van deze skills en
levert bewijs aan voor zijn/haar functioneren.
De beginnend beroepsbeoefenaar stelt ten behoeve van een uit te
voeren onderzoek (een adviesrapport met verbetervoorstellen) een plan
van aanpak (in correct Nederlands, B2) op. Het plan van aanpak geeft
richting aan het uit te voeren onderzoek en heeft als doel de
opdrachtgever te informeren over het uit te voeren onderzoek. Het plan
Opstellen Plan van Aanpak

van aanpak omvat:
• de aanleiding en achtergrond van het onderzoek,
• de doelstelling,
• de probleemstelling,
• onderzoeksvraag en deelvragen
• een onderbouwde onderzoeksopzet
• geeft aan wat zijn onderzoekspopulatie is;
• voegt definities toe van begrippen die behoren tot vakjargon
• geeft aan welke bronnen hij gaat gebruiken
• randvoorwaarden en beperkingen
De beginnend beroepsbeoefenaar benoemt op basis van een
systematische diagnose van een (deel)commercieel proces de
knelpunten en formuleert verbetervoorstellen in een adviesrapport
waarin verbetervoorstellen zijn gesignaleerd en concreet uitgewerkt.
De beginnend beroepsbeoefenaar voert een evaluatieonderzoek uit met
betrekking tot een recent doorgevoerde maatregel binnen het bedrijf dat

Opstellen van een

betrekking heeft op een sales vraagstuk, rapporteert hierover en kan de

Adviesrapport

gemaakte keuzes verdedigen en toelichten. In het evaluatieonderzoek
worden de volgende fasen onderscheiden:
• beschrijving van recent doorgevoerde maatregel/beleid voorzien van
achtergrond, de verandering en relatie met ontwikkelen van klantwaarde;
• toepassen van relevante theorie het inzetten van aanvullend onderzoek
met een beschrijving van de kritische succesfactoren;
• uitvoeren van een GAP analyse;
• opstellen van conclusies, aanbevelingen met een implementatieplan
inclusief financiële onderbouwing;
• reflectie op professionele ontwikkeling van het afgelopen ½ jaar.

138

Datum: 7 juli 2022
Pagina: 232 van 294

Bij de verdediging van het rapport beschikt de beginnend
beroepsbeoefenaar over goede uitdrukkingsvaardigheid en professionele
communicatie en is hij/zij in staat om:
• de gemaakte keuzes te verdedigen en een correct afweging te maken van
alternatieven;
• heldere toepassing te geven op gedane inzichten;
• kritisch te kunnen reflecteren op werkzaamheden en het onderzoek te
plaatsen in het grotere organisatorische geheel;
• voortschrijdend inzicht te onderbouwen.
Bij de diagnose en het formuleren van verbetervoorstellen neemt de
beginnend beroepsbeoefenaar de PDCA cyclus als uitgangspunt en
verzamelt aanvullende en verdiepende literatuur op het thema. De
nadruk voor het adviestraject ligt op de Check en Act fase van de PDCA
cyclus.
Voorbeelden van mogelijke Sales thema’s:
• Ontwikkelen of verbeteren van klantsegmentatie/accountbenadering
• Verbetering van de operationele ondersteuning van het salesproces
• Aanpassen van het bedieningsconcept voor een klantgroep
• Leadgeneratie verbeteren
• Leadopvolging verbeteren
• Klantportfoliomanagement invoeren/optimaliseren
De beginnend beroepsbeoefenaar reflecteert op het proces en product.
TENTAMINERING
Deeltentamen 1
Naam (deel)tentamen NL

<maximaal 120 posities in OSIRIS>

Naam (deel)tentamen EN

<maximaal 120 posities in OSIRIS>

Code (deel)tentamen OSIRIS

Vermeld het volgnummer van het tentamen of deeltentamen in OSIRIS.

Code en naam (deel)tentamen

SAEVWPS0A.5_D

Alluris

SA Werkplekscan

Wijze van aanmelden voor
(deel)tentamengelegenheden /
aanmeldingstermijn

Niet van toepassing

t/m 31 januari 2023
(via Alluris)
Intekenen en uittekenen voor
(deel)tentamengelegenheden
vanaf 1 februari 2023
(via OSIRIS)

Beschrijf de wijze van intekenen en uittekenen voor
(deel)tentamengelegenheden aangeboden vanaf 1 februari 2023
(implementatie Osiris) en bijbehorende termijnen. Voor meer informatie
zie Deel 3 ‘Regeling onderwijs en (deel)tentamens OSIRIS’.

Toegestane hulpmiddelen

Zie studiehandleiding op #OnderwijsOnline

Weging

0%

Omvat de leeruitkomst(en)

Werken en reflecteren op de werkplek volgens bedrijfsprotocol

139

Datum: 7 juli 2022
Pagina: 233 van 294

Tentamenvorm/ vormen
Tentamenmoment

Inleveropdracht, individueel
Regulier L1 / L3
Herkansing i.o.m. docent
De beginnend beroepsbeoefenaar beschikt over een relevante
werkplek.

Beoordelingscriteria
De beoordelingscriteria zijn nader uitgewerkt in de handleiding en het
beoordelingsformulier op #OnderwijsOnline.
Minimaal oordeel deeltentamen

V

Deeltentamen 2
Naam (deel)tentamen NL

<maximaal 120 posities in OSIRIS>

Naam (deel)tentamen EN

<maximaal 120 posities in OSIRIS>

Code (deel)tentamen OSIRIS

Vermeld het volgnummer van het tentamen of deeltentamen in OSIRIS.

Code en naam (deel)tentamen

SAEVPA0A.5_D

Alluris

SA Plan van Aanpak Sales

Wijze van aanmelden voor
(deel)tentamengelegenheden /
aanmeldingstermijn

Niet van toepassing

t/m 31 januari 2023
(via Alluris)
Intekenen en uittekenen voor

Beschrijf de wijze van intekenen en uittekenen voor

(deel)tentamengelegenheden

(deel)tentamengelegenheden aangeboden vanaf 1 februari 2023

vanaf 1 februari 2023

(implementatie Osiris) en bijbehorende termijnen. Voor meer informatie

(via OSIRIS)

zie Deel 3 ‘Regeling onderwijs en (deel)tentamens OSIRIS’.

Toegestane hulpmiddelen

Zie studiehandleiding op #OnderwijsOnline

Weging

20%

Omvat de leeruitkomst(en)

Opstellen Plan van aanpak

Tentamenvorm/ vormen

Inleveropdracht

Tentamenmoment

Reg Regulier L1 / L3
Herkansing iom docent

Beoordelingscriteria

De beginnend beroepsbeoefenaar stelt ten behoeve van een uit te
voeren onderzoek (een adviesrapport met verbetervoorstellen) een plan
van aanpak (in correct Nederlands, B2) op. Het plan van aanpak geeft
richting aan het uit te voeren onderzoek en heeft als doel de
opdrachtgever te informeren over het uit te voeren onderzoek. Het plan
van aanpak omvat:
• de aanleiding en achtergrond van het onderzoek,
• de doelstelling,
• de probleemstelling,
• onderzoeksvraag en deelvragen
• een onderbouwde onderzoeksopzet
• geeft aan wat zijn onderzoekspopulatie is;
• voegt definities toe van begrippen die behoren tot vakjargon
• geeft aan welke bronnen hij gaat gebruiken
• randvoorwaarden en beperkingen

140

Datum: 7 juli 2022
Pagina: 234 van 294
De beoordelingscriteria zijn nader uitgewerkt in de handleiding en het
beoordelingsformulier op #OnderwijsOnline.
Minimaal oordeel deeltentamen

5,5

Deeltentamen 3
Naam (deel)tentamen NL

<maximaal 120 posities in OSIRIS>

Naam (deel)tentamen EN

<maximaal 120 posities in OSIRIS>

Code (deel)tentamen OSIRIS

Vermeld het volgnummer van het tentamen of deeltentamen in OSIRIS.

Code en naam (deel)tentamen

SAEVWR0A.8_D

Alluris

SA Werken en reflecteren op de werkplek

Wijze van aanmelden voor

Niet van toepassing

(deel)tentamengelegenheden /
aanmeldingstermijn
t/m 31 januari 2023
(via Alluris)
Intekenen en uittekenen voor

Beschrijf de wijze van intekenen en uittekenen voor

(deel)tentamengelegenheden

(deel)tentamengelegenheden aangeboden vanaf 1 februari 2023

vanaf 1 februari 2023

(implementatie Osiris) en bijbehorende termijnen. Voor meer informatie

(via OSIRIS)

zie Deel 3 ‘Regeling onderwijs en (deel)tentamens OSIRIS’.

Toegestane hulpmiddelen

Zie studiehandleiding op #OnderwijsOnline

Weging

30%

Omvat de leeruitkomst(en)

Werken en reflecteren op de werkplek

Tentamenvorm/ vormen

Portfolio met CGI

Tentamenmoment

Regulier L2 / L4
Herkansing iom docent

Beoordelingscriteria

De beoordelingscriteria zijn nader uitgewerkt in de handleiding en het
beoordelingsformulier op #OnderwijsOnline.

Minimaal oordeel deeltentamen

5,5

Deeltentamen 4
Naam (deel)tentamen NL

<maximaal 120 posities in OSIRIS>

Naam (deel)tentamen EN

<maximaal 120 posities in OSIRIS>

Code (deel)tentamen OSIRIS

Vermeld het volgnummer van het tentamen of deeltentamen in OSIRIS.

Code en naam (deel)tentamen

SAEVAR0A.5_D

Alluris

SA Adviesrapport Sales

Wijze van aanmelden voor

Niet van toepassing

(deel)tentamengelegenheden /
aanmeldingstermijn
t/m 31 januari 2023
(via Alluris)
Intekenen en uittekenen voor

Beschrijf de wijze van intekenen en uittekenen voor

(deel)tentamengelegenheden

(deel)tentamengelegenheden aangeboden vanaf 1 februari 2023

vanaf 1 februari 2023

(implementatie Osiris) en bijbehorende termijnen. Voor meer informatie

(via OSIRIS)

zie Deel 3 ‘Regeling onderwijs en (deel)tentamens OSIRIS’.

Toegestane hulpmiddelen

Zie studiehandleiding op #OnderwijsOnline

Weging

50%

Omvat de leeruitkomst(en)

Opstellen van een adviesrapport

141

Datum: 7 juli 2022
Pagina: 235 van 294
Tentamenvorm/ vormen

Adviesrapport met CGI

Tentamenmoment

Regulier: L2 / L4
Herkansing iom docent

Beoordelingscriteria

De beoordelingscriteria zijn nader uitgewerkt in de handleiding en het
beoordelingsformulier op #OnderwijsOnline.

Minimaal oordeel deeltentamen

5,5

Minimaal oordeel EVL

6

8.1.3 Het Onderwijsarsenaal
In het onderwijsarsenaal beschrijft de opleiding per module welk onderwijs ze aanbiedt voor de
modules en eenheden van leeruitkomsten van de opleiding. In afstemming met je opleiding bepaal
je zelf of je wel of niet gebruik wilt maken van dit onderwijsaanbod. De keuze die je maakt wordt
opgenomen in de onderwijsovereenkomst.
Bij sommige modules en eenheden van leeruitkomsten is er een maximum gesteld aan het aantal
mensen dat kan deelnemen aan het onderwijsarsenaal. Als dat zo is, is dat vermeld bij de opbouw
van de betreffende module. Hetzelfde geldt voor de eigen financiële bijdrage. Als daar sprak e van
is bij het onderwijsarsenaal van een module of eenheid van leeruitkomsten, dan staat dat vermeld
bij de beschrijving van die module of eenheid van leeruitkomsten. Als er geen maximum aantal
deelnemers of een eigen bijdrage zijn vermeld, zijn die dus niet van toepassing.
Hieronder vind je per module eerst de opbouw van de module en daarna het onderwijsarsenaal dat
bij die module en de daaronder vallende eenheden van leeruitkomsten wordt aangeboden .
Junior Marketeer

JM

Junior Marketeer (max 120 posities in OSIRIS)

Naam module Engelstalig

<maximaal 120 posities in OSIRIS>
Naam EVL
1
2

Overzicht van EVL’en
waaruit de module is

3

opgebouwd

4

Deelnameplicht onderwijs
Taal indien anders dan
Nederlands
Inrichtingsvorm

JMMK Marketing &
Dienstenmarketing
JMPB marktsegmentatie, persona
& briefing
JMOM Marketingcommunicatie en
online marketing
JMCJ Online marketing
& de Customer Journey

Aantal studiepunten
5
5
5
5

5

JMNE Nederlands

2,5

6

JMPS Professional skills Canvas

7,5

Nee
Niet van toepassing
Deeltijd / Duaal

Beschrijving van de context van deze module
De beginnend beroepsbeoefenaar functioneert in een MKB of groot bedrijf. Hij/zij werkt in een
multidisciplinaire commerciële setting onder supervisie. De situatie en taken zijn gestructureerd. Hij/zij past
beproefde standaardmethoden toe die gebruikelijk zijn in de branche. De beginnend beroepsbeoefenaar

142

