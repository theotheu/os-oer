Datum: 7 juli 2022
Pagina: 204 van 294

Digital Advisor
DG

Digital Advisor (max 120 posities in OSIRIS)

Naam module
Engelstalig

<maximaal 120 posities in OSIRIS>

Ingangseisen

Voor de opleiding Bachelor Commerciële Economie / Associate Degree
Commerciële Economie duaal/deeltijd / Associate Degree Online Marketing
& Sales duaal/deeltijd is het verplicht een relevante werkplek te hebben. De
werkplek wordt beoordeeld aan de hand van een werkplekscan. Voldoet de
werkplek niet aan de eisen van de werkplekscan, is het niet mogelijk deze
module te volgen. De opleiding beoordeelt of de werkplek voldoet aan de
eisen van de opleiding.
Een duaal student dient te beschikken over een arbeids- of
stageovereenkomst. Een deeltijd student dient te beschikken over een
arbeids- of stageovereenkomst of bewijs van vrijwilligerswerk.
De professional skills worden uitgevoerd bij het bedrijf waar de duaal of
deeltijd beginnend beroepsbeoefenaar werkzaam is. Naast werkzaamheden
op een marketing- of salesafdeling worden een of meer relevante
opdrachten zelfstandig uitgevoerd.
Overige ingangseisen:
• Ad CE / OMS: minimaal 75 studiepunten behaald,
• Behaalde EvL’en:
•
Professional skills van JM, BS en OL en
•
ACVP,
• Maximaal 3 toetsen open (openstaande toetsen moeten tijdens module
afgerond kunnen worden).
Eventuele uitzonderingen hierop zijn altijd in overleg met en na goedkeuring
van de studieadviseur.

Naam EVL
Overzicht van EVL’en
waaruit de module is
opgebouwd

1
2
3
4

Deelnameplicht
onderwijs
Taal indien anders dan
Nederlands
Inrichtingsvorm

DGCO Conversie Optimalisatie
DGCJ Customer Journey mapping

Aantal
studiepunten
5

en personalisatie

5

DGKO Klanttevredenheidsonderzoek

5
15

DGEV Evaluatieonderzoek

Nee
Niet van toepassing
Deeltijd / Duaal
Bachelor Commerciële Economie

Afstudeerrichting/
afstudeervariant
Honoursprogramma/

Associate Degree Commerciële Economie / Online Marketing &
Sales: uitstroomdifferentiatie Online Marketing
Niet van toepassing

talentenprogramma
Beschrijving van de context van deze module
De beginnend beroepsbeoefenaar functioneert in een MKB of groot bedrijf. Hij/zij werkt in een multidisciplinaire
commerciële setting zelfstandig en zelfsturend. De situatie en taken zijn complex en (deels) gestructureerd.
Hij/zij past beproefde standaardmethoden toe die gebruikelijk zijn in de branche. De beginnend
beroepsbeoefenaar gebruikt hierbij theoretische kennis en vaardigheden en past deze aan de praktijk
aan. De beginnend beroepsbeoefenaar werkt afdelingsoverstijgend in verschillende projectteams
De beginnend beroepsbeoefenaar oriënteert zich op beroep en functie, pas t theoretische kennis toe op
commercieel- economisch gebied, oefent en ontwikkelt de skills van de commercieel professional, verkrijgt

111

Datum: 7 juli 2022
Pagina: 205 van 294
een beeld van de werkzaamheden en werksituaties van medewerkers, verkrijgt een beeld van de structuur
en het functioneren van een arbeidsorganisatie.

112

Datum: 7 juli 2022
Pagina: 206 van 294

DGCO Conversie Optimalisatie
EVL 1 – DGCO Conversie Optimalisatie [maximaal 120 posities in OSIRIS]
Naam EVL lang EN

[maximaal 120 posities in OSIRIS]

Naam EVL kort NL

[maximaal 40 posities in OSIRIS]

Naam EVL kort EN

[maximaal 40 posities in OSIRIS]

Naam EVL Alluris

DGCO Conversie Optimalisatie

Code EVL OSIRIS
Code EVL Alluris

Vermeld de code die in OSIRIS aan deze eenheid van leeruitkomsten is
gekoppeld. [XXXXXnn (6 letterige code + 2 cijfers in het volgnummer)]
DGCO
Opleidingsprofiel CE 2018-2022
Koers bepalen
x

Eindkwalificatie(s)

Aantal studiepunten
Ingangseisen EVL
Intekenen onderwijsarsenaal

Business Development

x

Waarde creëren

x

Realiseren

x

5
De ingangseisen zijn op module niveau geformuleerd, niet op EVL
niveau.
Voor het onderwijsarsenaal dat wordt aangeboden na 31 januari 2023
geldt dat het noodzakelijk is dat studenten zich aanmelden voor het
onderwijs dat zij willen volgen (ook wel ‘intekenen’ genoemd). Zie Deel 3
‘Regeling onderwijs en (deel)tentamens OSIRIS’ voor meer informatie.

Beschrijving van de context van deze EVL
De beginnend beroepsbeoefenaar functioneert in een MKB of groot bedrijf. Hij/zij werkt in een
multidisciplinaire commerciële setting zelfstandig en zelfsturend. De situatie en taken zijn complex en (deels)
gestructureerd. Hij/zij past beproefde standaardmethoden toe die gebruikelijk zijn in de branche. De
beginnend beroepsbeoefenaar gebruikt hierbij theoretische kennis en vaardigheden en past deze aan de
praktijk aan. De beginnende beroepsbeoefenaar werkt afdeling overstijgend in verschillende projectteams.
De beginnend beroepsbeoefenaar oriënteert zich op beroep en functie, past theoretische kenn is toe op
commercieel- economisch gebied, oefent en ontwikkelt de skills van de commercieel professional, verkrijgt
een beeld van de werkzaamheden en werksituaties van medewerkers, verkrijgt een beeld van de structuur
en het functioneren van een arbeidsorganisatie
Beschrijving van de leeruitkomst(en) waaruit deze EVL is opgebouwd

Consumentengedrag &
Gedragsanalyse

De beginnend beroepsbeoefenaar zet onderzoek en analyses op het gebied
van consumentengedrag en gedragsanalyse op een onderbouwde en
transparante manier in om te komen met nieuwe ideeën. Op grond van het
onderzoek en analyse formuleert hij/zij hypotheses en stelt een
implementatieplan voor verbeteringen op. De voorgestelde verbeteringen
worden daadwerkelijk geïmplementeerd in een website en/of social media
kanaal. Het resultaat wordt door middel van een zakelijke adviserende
presentatie die mede gericht is op concrete implementatie van de
voorgestelde verbeteringen aan een externe stakeholder gepresenteerd.

113

Datum: 7 juli 2022
Pagina: 207 van 294
Om bovengenoemde leeruitkomsten te behalen:
• beheerst hij de basiskennis behavioural economics;
• kan hij consumentengedrag specifiek gericht op conversie analyseren
en voorspellen aan de hand van bestaande modellen;
• is hij in staat dataverwerking uit te voeren en analytics tools toe te passen
en te beoordelen;
• kan hij een cijfermatige analyse van gebruikersgedrag uitvoeren;
• kan hij draaitabellen construeren met als doel analyse van gebruiksdata;
• is hij in staat conclusies te trekken over de toepassing van deze
gedragsregels in de praktijk, voert aanvullend kwalitatief onderzoek
uit en stelt op basis daarvan hypotheses en een
implementatieplan voor verbeteringen op;
• kan hij de voorgestelde verbeteringen daadwerkelijk implementeren in
een website en/of social media kanaal.
De beginnend beroepsbeoefenaar is in staat een testplan op te stellen om
verbeterhypotheses in de praktijk te testen. Hij/zij is in staat het testplan uit te
voeren en conclusies hieraan te verbinden. Het testplan en de testuitkomsten
worden middels een zakelijke presentatie aan externe stakeholders
gepresenteerd.
Experimenteren en Testen

De beginnend beroepsbeoefenaar
• kan experimenten en A/B testen voor o.a. websites, email
en social media uitvoeren en deze analyseren;
• is in staat een rapportage van experimenten en tests uit te voeren en
hierover te rapporteren.

TENTAMINERING
Deeltentamen 1
Naam (deel)tentamen NL

<maximaal 120 posities in OSIRIS>

Naam (deel)tentamen EN

<maximaal 120 posities in OSIRIS>

Code (deel)tentamen OSIRIS

Vermeld het volgnummer van het tentamen of deeltentamen in OSIRIS.

Code en naam (deel)tentamen

DGCOG0A.6_D

Alluris

Gedragsanalyse

Wijze van aanmelden voor
(deel)tentamengelegenheden /
aanmeldingstermijn

Niet van toepassing

t/m 31 januari 2023
(via Alluris)
Intekenen en uittekenen voor
(deel)tentamengelegenheden
vanaf 1 februari 2023
(via OSIRIS)

Beschrijf de wijze van intekenen en uittekenen voor
(deel)tentamengelegenheden aangeboden vanaf 1 februari 2023
(implementatie Osiris) en bijbehorende termijnen. Voor meer informatie
zie Deel 3 ‘Regeling onderwijs en (deel)tentamens OSIRIS’.

Toegestane hulpmiddelen

Zie handleiding op #OnderwijsOnline

Weging

66%

Omvat de leeruitkomst(en)

Consumentengedrag & Gedragsanalyse

Tentamenvorm/ vormen

Individueel, Presentatie

Tentamenmoment

Regulier: L1 / L3
Herkansing i.o.m. docent

114

Datum: 7 juli 2022
Pagina: 208 van 294

Beoordelingscriteria
Minimaal oordeel deeltentamen

De beoordelingscriteria zijn nader uitgewerkt in de handleiding en het
beoordelingsformulier op onderwijsonline.
5,5

Deeltentamen 2
Naam (deel)tentamen NL

<maximaal 120 posities in OSIRIS>

Naam (deel)tentamen EN

<maximaal 120 posities in OSIRIS>

Code (deel)tentamen OSIRIS

Vermeld het volgnummer van het tentamen of deeltentamen in OSIRIS.

Code en naam (deel)tentamen

DGCOTV0A.6_D

Alluris

Testverslag

Wijze van aanmelden voor

Niet van toepassing

(deel)tentamengelegenheden /
aanmeldingstermijn
t/m 31 januari 2023
(via Alluris)
Intekenen en uittekenen voor

Beschrijf de wijze van intekenen en uittekenen voor

(deel)tentamengelegenheden

(deel)tentamengelegenheden aangeboden vanaf 1 februari 2023

vanaf 1 februari 2023

(implementatie Osiris) en bijbehorende termijnen. Voor meer informatie

(via OSIRIS)

zie Deel 3 ‘Regeling onderwijs en (deel)tentamens OSIRIS’.

Toegestane hulpmiddelen

Zie studiehandleiding op #OnderwijsOnline

Weging

34%

Omvat de leeruitkomst(en)

Experimenteren & Testen

Tentamenvorm/ vormen

Presentatie, individueel

Tentamenmoment

Regulier: L2 / L4
Herkansing i.o.m. docent

Beoordelingscriteria

De beoordelingscriteria zijn nader uitgewerkt in de handleiding en het
beoordelingsformulier op #OnderwijsOnline.

Minimaal oordeel deeltentamen

5,5

Minimaal oordeel EVL

6

115

Datum: 7 juli 2022
Pagina: 209 van 294

DGCJ Omnichannel customer journey mapping en personalisatie
EVL 2 – DGCJ Omnichannel customer journey mapping en personalisatie [maximaal 120 posities in
OSIRIS]
Naam EVL lang EN

[maximaal 120 posities in OSIRIS]

Naam EVL kort NL

[maximaal 40 posities in OSIRIS]

Naam EVL kort EN

[maximaal 40 posities in OSIRIS]

Naam EVL Alluris

Omnichannel customer journey mapping en personalisatie

Code EVL OSIRIS
Code EVL Alluris

Vermeld de code die in OSIRIS aan deze eenheid van leeruitkomsten is
gekoppeld. [XXXXXnn (6 letterige code + 2 cijfers in het volgnummer)]
DGJC
Opleidingsprofiel CE 2018-2022
Koers bepalen
x

Eindkwalificatie(s)

Aantal studiepunten
Ingangseisen EVL
Intekenen onderwijsarsenaal

Business Development

x

Waarde creëren

x

Realiseren

x

5
De ingangseisen zijn op module niveau geformuleerd, niet op EVL
niveau.
Voor het onderwijsarsenaal dat wordt aangeboden na 31 januari 2023
geldt dat het noodzakelijk is dat studenten zich aanmelden voor het
onderwijs dat zij willen volgen (ook wel ‘intekenen’ genoemd). Zie Deel 3
‘Regeling onderwijs en (deel)tentamens OSIRIS’ voor meer informatie.

Beschrijving van de context van deze EVL
De beginnend beroepsbeoefenaar functioneert in een MKB of groot bedrijf. Hij/zij werkt in een
multidisciplinaire commerciële setting zelfstandig en zelfsturend. De situatie en taken zijn complex en (deels)
gestructureerd. Hij/zij past beproefde standaardmethoden toe die gebruikelijk zijn in de branche. De
beginnend beroepsbeoefenaar gebruikt hierbij theoretische kennis en vaardigheden en past deze aan de
praktijk aan. De beginnende beroepsbeoefenaar werkt afdeling overstijgend in verschillende projectteams.
De beginnend beroepsbeoefenaar oriënteert zich op beroep en functie, past theoretische kennis toe op
commercieel- economisch gebied, oefent en ontwikkelt de skills van de commercieel professional, verkrijgt
een beeld van de werkzaamheden en werksituaties van medewerkers, verkrijgt een beeld van de structuur
en het functioneren van een arbeidsorganisatie
Beschrijving van de leeruitkomst(en) waaruit deze EVL is opgebouwd

Customer journey design

De beginnend beroepsbeoefenaar heeft inzicht in huidige toepassing
van omnichannel online marketing van het bedrijf.
De beginnend beroepsbeoefenaar:
• onderzoekt de customer journey voor een organisatie, brengt
touchpoints in kaart en formuleert een verbetervoorstel met behulp van
customer journey mapping;
• benoemt omnichannel opportunities vanuit best practises en theorie en
vertaald naar de organisatie;

116

Datum: 7 juli 2022
Pagina: 210 van 294
• inzet van social media en social media monitoring krijgt specifieke
aandacht.
De beginnende beroepsbeoefenaar realiseert een nieuwe toepassing
van omnichannel online marketing. De beginnend beroepsbeoefenaar
werkt voor een tweetal journey’s nieuwe mogelijkheden uit voor een
omnichannel benadering van de klant.
Omnichannel psychologie
van de conversie

De beginnend beroepsbeoefenaar is in staat:
• optimalisatie van de customer journey door toepassen van human
centered design en gedragspsychologie;
• monitoren en inzet van social media;
• ontwikkelen op basis van sprints en agile werken;
• toepassen van designthinking.

TENTAMINERING
Deeltentamen 1
Naam (deel)tentamen NL

<maximaal 120 posities in OSIRIS>

Naam (deel)tentamen EN

<maximaal 120 posities in OSIRIS>

Code (deel)tentamen OSIRIS

Vermeld het volgnummer van het tentamen of deeltentamen in OSIRIS.

Code en naam (deel)tentamen

DGCJMP0A.6_D

Alluris

Customer Jouney design

Wijze van aanmelden voor
(deel)tentamengelegenheden /
aanmeldingstermijn

Niet van toepassing

t/m 31 januari 2023
(via Alluris)
Intekenen en uittekenen voor
(deel)tentamengelegenheden
vanaf 1 februari 2023
(via OSIRIS)

Beschrijf de wijze van intekenen en uittekenen voor
(deel)tentamengelegenheden aangeboden vanaf 1 februari 2023
(implementatie Osiris) en bijbehorende termijnen. Voor meer informatie
zie Deel 3 ‘Regeling onderwijs en (deel)tentamens OSIRIS’.

Toegestane hulpmiddelen

Zie studiehandleiding op #OnderwijsOnline

Weging

100%

Omvat de leeruitkomst(en)

Customer journey design

Tentamenvorm/ vormen

Presentatie (pitch) individueel

Tentamenmoment

Beoordelingscriteria

Regulier: L2 / L4
Herkansing i.o.m. docent
De beoordelingscriteria zijn nader uitgewerkt in de handleiding en het
beoordelingsformulier op onderwijsonline.

Minimaal oordeel deeltentamen

5,5

Minimaal oordeel EVL

6

117

Datum: 7 juli 2022
Pagina: 211 van 294

DGKO Klanttevredenheidsonderzoek
EVL 3 – DGKO Klanttevredenheidsonderzoek [maximaal 120 posities in OSIRIS]
Naam EVL lang EN

[maximaal 120 posities in OSIRIS]

Naam EVL kort NL

[maximaal 40 posities in OSIRIS]

Naam EVL kort EN

[maximaal 40 posities in OSIRIS]

Naam EVL Alluris

Klanttevredenheidsonderzoek

Code EVL OSIRIS
Code EVL Alluris

Vermeld de code die in OSIRIS aan deze eenheid van leeruitkomsten is
gekoppeld. [XXXXXnn (6 letterige code + 2 cijfers in het volgnummer)]
DGKO
Opleidingsprofiel CE 2018-2022
Koers bepalen
x
Business Development

Eindkwalificatie(s)

Waarde creëren
Realiseren

Aantal studiepunten
Ingangseisen EVL
Intekenen onderwijsarsenaal

x

5
De ingangseisen zijn op module niveau geformuleerd, niet op EVL
niveau.
Voor het onderwijsarsenaal dat wordt aangeboden na 31 januari 2023
geldt dat het noodzakelijk is dat studenten zich aanmelden voor het
onderwijs dat zij willen volgen (ook wel ‘intekenen’ genoemd). Zie Deel 3
‘Regeling onderwijs en (deel)tentamens OSIRIS’ voor meer informatie.

Beschrijving van de context van deze EVL
De beginnend beroepsbeoefenaar functioneert in een MKB of groot bedrijf. Hij/zij werkt in een
multidisciplinaire commerciële setting zelfstandig en zelfsturend. De situatie en taken zijn complex en (deels)
gestructureerd. Hij/zij past beproefde standaardmethoden toe die gebruikelijk zijn in de branche. De
beginnend beroepsbeoefenaar gebruikt hierbij theoretische kennis en vaardigheden en past deze aan de
praktijk aan. De beginnende beroepsbeoefenaar werkt afdeling overstijgend in verschillende projectteams.
De beginnend beroepsbeoefenaar oriënteert zich op beroep en functie, past theoretische kennis toe op
commercieel- economisch gebied, oefent en ontwikkelt de skills van de commercieel professional, verkrijgt
een beeld van de werkzaamheden en werksituaties van medewerkers, verkrijgt een beeld van de structuur
en het functioneren van een arbeidsorganisatie
Beschrijving van de leeruitkomst(en) waaruit deze EVL is opgebouwd

Klanttevredenheidsonderzoek opzetten,
uitvoeren, analyseren en
rapporteren

De beginnend beroepsbeoefenaar voert een klanttevredenheidsonderzoek
op een website of via email uit en rapporteert hierover in de vorm van een
verslag. Hij/zij komt met onderbouwde voorstellen voor duurzame waarde
creatie voor de klant en/of organisatie waarin tevens aanbevelingen voor
vervolg onderzoek zijn opgenomen.
De beginnend beroepsbeoefenaar is in staat:
• een plan van aanpak te schrijven voor het onderzoek;
• een klanttevredenheidsonderzoek op te zetten en uit te voeren;
• een NPS onderzoek (of vergelijkbare systematiek) uit te voeren;

118

Datum: 7 juli 2022
Pagina: 212 van 294
•
•

•

de verzamelde gegevens te analyseren met gebruikmaking van
statische analyse methoden;
correcte verslaglegging te doen van het onderzoek waarbij
tenminste is opgenomen:
o een inleiding die aangeeft wat de aanleiding, de
doelstelling, de probleemstelling, definities,
randvoorwaarden/beperkingen en de gehanteerde methode zijn
o de resultaten, inclusief een objectieve analyse
conclusies / aanbevelingen die gebaseerd zijn op de resultaten en
die aansluiten op de hoofdvraag; aanbevelingen zijn relevant voor
de opdrachtgever.

TENTAMINERING
Deeltentamen 1
Naam (deel)tentamen NL

<maximaal 120 posities in OSIRIS>

Naam (deel)tentamen EN

<maximaal 120 posities in OSIRIS>

Code (deel)tentamen OSIRIS

Vermeld het volgnummer van het tentamen of deeltentamen in OSIRIS.

Code en naam (deel)tentamen

DGKOKLT0A.5_D

Alluris

Klanttevredenheidsonderzoek

Wijze van aanmelden voor
(deel)tentamengelegenheden /
aanmeldingstermijn

Niet van toepassing

t/m 31 januari 2023
(via Alluris)
Intekenen en uittekenen voor
(deel)tentamengelegenheden
vanaf 1 februari 2023
(via OSIRIS)

Beschrijf de wijze van intekenen en uittekenen voor
(deel)tentamengelegenheden aangeboden vanaf 1 februari 2023
(implementatie Osiris) en bijbehorende termijnen. Voor meer informatie
zie Deel 3 ‘Regeling onderwijs en (deel)tentamens OSIRIS’.

Toegestane hulpmiddelen

Zie studiehandleiding op #OnderwijsOnline

Weging

100%

Omvat de leeruitkomst(en)
Tentamenvorm/ vormen
Tentamenmoment

Beoordelingscriteria

Klanttevredenheidsonderzoek opzetten, uitvoeren, analyseren en
rapporteren
Inleveropdracht
Regulier: L2 / L4
Herkansing i.o.m. docent
De beoordelingscriteria zijn nader uitgewerkt in de handleiding en het
beoordelingsformulier op onderwijsonline.

Minimaal oordeel deeltentamen

5,5

Minimaal oordeel EVL

6

119

Datum: 7 juli 2022
Pagina: 213 van 294

DGEV Evaluatieopdracht Online Marketing
EVL 4 – DGEV Evaluatieopdracht Online Marketing [maximaal 120 posities in OSIRIS]
Naam EVL lang EN

[maximaal 120 posities in OSIRIS]

Naam EVL kort NL

[maximaal 40 posities in OSIRIS]

Naam EVL kort EN

[maximaal 40 posities in OSIRIS]

Naam EVL Alluris

Evaluatieopdracht Online Marketing

Code EVL OSIRIS
Code EVL Alluris

Vermeld de code die in OSIRIS aan deze eenheid van leeruitkomsten is
gekoppeld. [XXXXXnn (6 letterige code + 2 cijfers in het volgnummer)]
DGEV
Opleidingsprofiel CE 2018-2022
Koers bepalen
x

Eindkwalificatie(s)

Aantal studiepunten
Ingangseisen EVL
Intekenen onderwijsarsenaal

Business Development

x

Waarde creëren

x

Realiseren

x

15
De ingangseisen zijn op module niveau geformuleerd, niet op EVL
niveau.
Voor het onderwijsarsenaal dat wordt aangeboden na 31 januari 2023
geldt dat het noodzakelijk is dat studenten zich aanmelden voor het
onderwijs dat zij willen volgen (ook wel ‘intekenen’ genoemd). Zie Deel 3
‘Regeling onderwijs en (deel)tentamens OSIRIS’ voor meer informatie.

Beschrijving van de context van deze EVL
De beginnend beroepsbeoefenaar functioneert in een MKB of groot bedrijf. Hij/zij werkt in een
multidisciplinaire commerciële setting zelfstandig en zelfsturend. De situatie en taken zijn complex en (deels)
gestructureerd. Hij/zij past beproefde standaardmethoden toe die gebruikelijk zij n in de branche. De
beginnend beroepsbeoefenaar gebruikt hierbij theoretische kennis en vaardigheden en past deze aan de
praktijk aan. De beginnende beroepsbeoefenaar werkt afdeling overstijgend in verschillende projectteams.
De beginnend beroepsbeoefenaar oriënteert zich op beroep en functie, past theoretische kennis toe op
commercieel- economisch gebied, oefent en ontwikkelt de skills van de commercieel professional, verkrijgt
een beeld van de werkzaamheden en werksituaties van medewerkers, verkrijgt ee n beeld van de structuur
en het functioneren van een arbeidsorganisatie
Beschrijving van de leeruitkomst(en) waaruit deze EVL is opgebouwd

Werken en reflecteren op de
werkplek

De beginnend beroepsbeoefenaar functioneert in een MKB of groot bedrijf in
een commerciële setting. Hierbij heeft hij/zij heeft oog voor verschillende
belangen binnen het bedrijf en kan draagvlak creëren. Hij/zij communiceert
goed, zowel verbaal als schriftelijk. Hij/zij heeft oog voor detail en plant zijn
eigen werk zorgvuldig zodat zijn werk af is. Hij/zij is in staat onder hoge druk
en binnen gestelde deadlines naar het resultaat toe te werken binnen en
gestructureerde context en met voldoende begeleiding. Hij/zij is sensitief voor
het maken van de verbinding op inhoud- en mensniveau binnen zijn eigen
omgeving. Hij beschikt over een ondernemende houding.

120

Datum: 7 juli 2022
Pagina: 214 van 294
De beginnend beroepsbeoefenaar kan zijn/haar droombaan over 3 jaar
beschrijven en weet daarbij aan te geven welke van de onderstaande skills
uit het opleidingsprofiel CE het meest van toepassing zijn. Het gaat hierbij
om de skills:
• Kritisch denken / probleemoplossend vermogen
• Creativiteit
• Communicatie
• Samenwerken
• Nieuwsgierigheid
• Initiatief
• Doorzettingsvermogen
• aanpassingsvermogen
• Leiderschap
• Verantwoordelijkheidsbesef
• Commercieel bewustzijn
• Hij/zij werkt doelbewust aan verdere ontwikkeling van deze skills en
levert bewijs aan voor zijn/haar functioneren
De beginnend beroepsbeoefenaar stelt ten behoeve van een uit te
voeren onderzoek (een adviesrapport met verbetervoorstellen) een plan
van aanpak (in correct Nederlands, B2) op. Het plan van aanpak geeft
richting aan het uit te voeren onderzoek en heeft als doel de
opdrachtgever te informeren over het uit te voeren onderzoek. Het plan
Opstellen Plan van Aanpak

van aanpak omvat:
• een evaluatie die de aanleiding en achtergrond van het onderzoek vormt
• de doelstelling
• de probleemstelling
• onderzoeksvraag en deelvragen
• een onderbouwde onderzoeksopzet
• randvoorwaarden en beperkingen

121

Datum: 7 juli 2022
Pagina: 215 van 294

De beginnend beroepsbeoefenaar benoemt op basis van een
systematische diagnose van een (deel)commercieel proces de
knelpunten en formuleert verbetervoorstellen in een adviesrapport
waarin verbetervoorstellen zijn gesignaleerd en concreet uitgewerkt.
De beginnend beroepsbeoefenaar voert een evaluatieonderzoek uit met
betrekking tot een recent doorgevoerde maatregel binnen het bedrijf dat
betrekking heeft op een online marketing vraagstuk, rapporteert hierover
en kan de gemaakte keuzes verdedigen en toelichten.

Adviesrapport opstellen en
verdedigen

In het evaluatieonderzoek worden de volgende fasen onderscheiden:
• Beschrijving van recent doorgevoerde maatregel/beleid voorzien van
achtergrond, de verandering en relatie met ontwikkelen van
klantwaarde.
• Toepassen van relevante theorie het inzetten van aanvullend
onderzoek met een beschrijving van de kritische succesfactoren.
• Uitvoeren van een GAP analyse.
• Opstellen van conclusies, aanbevelingen met een implementatieplan
inclusief financiële onderbouwing.
• Reflectie op professionele ontwikkeling van het afgelopen ½ jaar op
het proces en het product.
Bij de verdediging van het rapport beschikt de beginnend
beroepsbeoefenaar over goede uitdrukkingsvaardigheden en
professionele communicatie en is hij/zij in staat om:
• De gemaakte keuzes te verdedigen en een correcte afweging te
maken van alternatieven.
• Heldere toepassing te geven op gedane inzichten.
• Kritisch te kunnen reflecteren op werkzaamheden en het onderzoek
te plaatsen in het grotere organisatorische geheel.
• Voortschrijdend inzicht te onderbouwen.

TENTAMINERING
Deeltentamen 1
Naam (deel)tentamen NL

<maximaal 120 posities in OSIRIS>

Naam (deel)tentamen EN

<maximaal 120 posities in OSIRIS>

Code (deel)tentamen OSIRIS

Vermeld het volgnummer van het tentamen of deeltentamen in OSIRIS.

Code en naam (deel)tentamen

DGEVWPS0A.5_D

Alluris

DG Werkplekscan

Wijze van aanmelden voor
(deel)tentamengelegenheden /
aanmeldingstermijn

Niet van toepassing

t/m 31 januari 2023
(via Alluris)
Intekenen en uittekenen voor
(deel)tentamengelegenheden
vanaf 1 februari 2023
(via OSIRIS)
Toegestane hulpmiddelen

Beschrijf de wijze van intekenen en uittekenen voor
(deel)tentamengelegenheden aangeboden vanaf 1 februari 2023
(implementatie Osiris) en bijbehorende termijnen. Voor meer informatie
zie Deel 3 ‘Regeling onderwijs en (deel)tentamens OSIRIS’.

Zie handleiding op #OnderwijsOnline

122

Datum: 7 juli 2022
Pagina: 216 van 294

Weging

0%

Omvat de leeruitkomst(en)

Werken en reflecteren op de werkplek volgens bedrijfsprotocol

Tentamenvorm/ vormen

Inleveropdracht, individueel

Tentamenmoment

Beoordelingscriteria
Minimaal oordeel deeltentamen

Regulier L1 / L3
Herkansing i.o.m. docent
De beoordelingscriteria zijn nader uitgewerkt in de handleiding en het
beoordelingsformulier op onderwijsonline.
V

Deeltentamen 2
Naam (deel)tentamen NL

<maximaal 120 posities in OSIRIS>

Naam (deel)tentamen EN

<maximaal 120 posities in OSIRIS>

Code (deel)tentamen OSIRIS

Vermeld het volgnummer van het tentamen of deeltentamen in OSIRIS.

Code en naam (deel)tentamen

DGEVPVA0A.5_D

Alluris

DG Plan van Aanpak Online marketing

Wijze van aanmelden voor

Niet van toepassing

(deel)tentamengelegenheden /
aanmeldingstermijn
t/m 31 januari 2023
(via Alluris)
Intekenen en uittekenen voor

Beschrijf de wijze van intekenen en uittekenen voor

(deel)tentamengelegenheden

(deel)tentamengelegenheden aangeboden vanaf 1 februari 2023

vanaf 1 februari 2023

(implementatie Osiris) en bijbehorende termijnen. Voor meer informatie

(via OSIRIS)

zie Deel 3 ‘Regeling onderwijs en (deel)tentamens OSIRIS’.

Toegestane hulpmiddelen

Zie studiehandleiding op #OnderwijsOnline

Weging

20%

Omvat de leeruitkomst(en)

Opstellen Plan van aanpak

Tentamenvorm/ vormen

Inleveropdracht, individueel

Tentamenmoment

Regulier L1 / L3
Herkansing iom docent

Beoordelingscriteria

De beginnend beroepsbeoefenaar schrijft een PvA voor een onderzoek
(in correct Nederlands, B2) waarin opgenomen is:

• de aanleiding van het onderzoek, gebaseerd op de briefing
• de doelstelling,
• de probleemstelling,
• een onderbouwde methode
• verantwoordt zijn keuze voor de onderzoeksmethode(n);
• geeft aan wat zijn onderzoekspopulatie is;
• geeft (eventueel) aan welke type steekproef hij gaat trekken;
• geeft aan naar welk betrouwbaarheid en onnauwkeurigheid hij streeft in
het onderzoek;

• beschrijft hoe hij het afnemen van de vragenlijst gaat organiseren
• voegt definities toe van begrippen die behoren tot vakjargon
• geeft aan welke bronnen hij gaat gebruiken
• randvoorwaarden en beperkingen

123

Datum: 7 juli 2022
Pagina: 217 van 294
De beoordelingscriteria zijn nader uitgewerkt in de handleiding en het
beoordelingsformulier op onderwijsonline.
Minimaal oordeel deeltentamen

5,5

Deeltentamen 3
Naam (deel)tentamen NL

<maximaal 120 posities in OSIRIS>

Naam (deel)tentamen EN

<maximaal 120 posities in OSIRIS>

Code (deel)tentamen OSIRIS

Vermeld het volgnummer van het tentamen of deeltentamen in OSIRIS.

Code en naam (deel)tentamen

DGEVWR0A.8_D

Alluris

DG Werken en reflecteren op de werkplek

Wijze van aanmelden voor

Niet van toepassing

(deel)tentamengelegenheden /
aanmeldingstermijn
t/m 31 januari 2023
(via Alluris)
Intekenen en uittekenen voor

Beschrijf de wijze van intekenen en uittekenen voor

(deel)tentamengelegenheden

(deel)tentamengelegenheden aangeboden vanaf 1 februari 2023

vanaf 1 februari 2023

(implementatie Osiris) en bijbehorende termijnen. Voor meer informatie

(via OSIRIS)

zie Deel 3 ‘Regeling onderwijs en (deel)tentamens OSIRIS’.

Toegestane hulpmiddelen

Zie studiehandleiding op #OnderwijsOnline

Weging

30%

Omvat de leeruitkomst(en)

Werken en reflecteren op de werkplek

Tentamenvorm/ vormen

Portfolio met CGI

Tentamenmoment

Regulier L2 / L4
Herkansing iom docent

Beoordelingscriteria

De beoordelingscriteria zijn nader uitgewerkt in de handleiding en het
beoordelingsformulier op onderwijsonline.

Minimaal oordeel deeltentamen

5,5

Deeltentamen 4
Naam (deel)tentamen NL

<maximaal 120 posities in OSIRIS>

Naam (deel)tentamen EN

<maximaal 120 posities in OSIRIS>

Code (deel)tentamen OSIRIS

Vermeld het volgnummer van het tentamen of deeltentamen in OSIRIS.

Code en naam (deel)tentamen

DGEVAR0A.5_D

Alluris

DG Adviesrapport Online marketing

Wijze van aanmelden voor

Niet van toepassing

(deel)tentamengelegenheden /
aanmeldingstermijn
t/m 31 januari 2023
(via Alluris)
Intekenen en uittekenen voor

Beschrijf de wijze van intekenen en uittekenen voor

(deel)tentamengelegenheden

(deel)tentamengelegenheden aangeboden vanaf 1 februari 2023

vanaf 1 februari 2023

(implementatie Osiris) en bijbehorende termijnen. Voor meer informatie

(via OSIRIS)

zie Deel 3 ‘Regeling onderwijs en (deel)tentamens OSIRIS’.

Toegestane hulpmiddelen

Zie studiehandleiding op #OnderwijsOnline

Weging

50%

124

Datum: 7 juli 2022
Pagina: 218 van 294
Omvat de leeruitkomst(en)

Opstellen van een adviesrapport

Tentamenvorm/ vormen

Adviesrapport met CGI, individueel

Tentamenmoment

Regulier: L2 / L4
Herkansing iom docent

Beoordelingscriteria

De beoordelingscriteria zijn nader uitgewerkt in de handleiding en het
beoordelingsformulier op onderwijsonline.

Minimaal oordeel deeltentamen

5,5

Minimaal oordeel EVL

6

Sales Manager
SA

Sales Manager (max 120 posities in OSIRIS)

Naam module Engelstalig

<maximaal 120 posities in OSIRIS>

Ingangseisen

Voor de opleiding Bachelor Commerciële Economie / Associate Degree
Commerciële Economie duaal/deeltijd / Associate Degree Online Marketing
& Sales duaal/deeltijd is het verplicht een relevante werkplek te hebben. De
werkplek wordt beoordeeld aan de hand van een werkplekscan. Voldoet de
werkplek niet aan de eisen van de werkplekscan, is het niet mogelijk deze
module te volgen. De opleiding beoordeelt of de werkplek voldoet aan de
eisen van de opleiding.
Een duaal student dient te beschikken over een arbeids- of
stageovereenkomst.
Een deeltijd student dient te beschikken over een arbeids-, of
stageovereenkomst of bewijs van vrijwilligerswerk.
De professional skills worden uitgevoerd bij het bedrijf waar de duaal of
deeltijd beginnend beroepsbeoefenaar werkzaam is. Naast werkzaamheden
op een marketing- of salesafdeling worden een of meer relevante
opdrachten zelfstandig uitgevoerd.
Overige ingangseisen:
• studievoortgangsnorm behaald,
• Ad CE/OMS: minimaal 75 studiepunten behaald
• behaalde EvL’en:
o Professional skills van JM, BS en AC en
o ACVP,
• maximaal 3 toetsen open (openstaande toetsen moeten tijdens module
afgerond kunnen worden).
Eventuele uitzonderingen hierop zijn altijd in overleg met en na
goedkeuring van de studieadviseur.

Naam EVL

Aantal
studiepunten

Overzicht van EVL’en

1

SAJB Jaarbudget

5

waaruit de module is

2

SAAR Adviesrapport

5

opgebouwd

3

SAVG Voortgangsgesprek

2,5

4

SATO Teamoverleg

2,5

5

SAEV Evaluatieopdracht

15

Deelnameplicht onderwijs
Taal indien anders dan
Nederlands
Inrichtingsvorm

Nee
Niet van toepassing
Deeltijd / Duaal

125

