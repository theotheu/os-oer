import os
from os import listdir
from os.path import isfile, join
import csv


def import_doc(fullpath):
    with open(fullpath, 'r', encoding='utf-8') as f:
        lines = f.readlines()
    doc = ''.join(lines)
    doc = doc.replace('\n', ' ')
    return doc


def read_docs():
    docs = {}
    base_dir = './HvA/txt'
    files = [f for f in listdir(base_dir) if isfile(join(base_dir, f))]
    for file in files:
        if '.txt' in file:
            fullpath = os.path.join(base_dir, file)
            doc = import_doc(fullpath)
            docs[file] = doc
    return docs


def count_words(terms, docs):
    results = {}
    for filename in docs:
        doc = docs[filename]
        # print(filename)
        total = 0
        occurrences = {}
        for term in terms:
            times = doc.count(term)
            total += times
            occurrences[term] = times
        occurrences['total'] = total
        results[filename] = occurrences
    return results


def print_results(terms, results):
    lines = []

    header = ['filename', 'total']
    for term in terms:
        header.append(term)
    lines.append(header)

    for filename in results:
        line = [filename.replace('.txt', '.pdf')]
        p = results[filename]
        line.append(p['total'])
        for pp in p:
            if 'total' not in pp:
                line.append(p[pp])

        lines.append(line)

    return lines


def write_to_file(lines):
    f = open('results.csv', 'w')
    writer = csv.writer(f)
    for line in lines:
        writer.writerow(line)
    f.close()


if __name__ == '__main__':
    terms = ['ethisch', 'ethische', 'ethiek', 'ethic', 'ethics', 'ethical', 'filosofisch', 'filosofische', 'filosofie', 'philosophical', 'philosophy', 'moreel', 'morele', 'moral', 'oordeelsvorming', 'metafysica', '(meta)fysica', '(meta)fysisch', '(meta)fysische']
    docs = read_docs()
    results = count_words(terms, docs)
    lines = print_results(terms, results)
    write_to_file(lines)
